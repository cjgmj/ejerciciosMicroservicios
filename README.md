# ejerciciosMicroservicios

## Zipkin
1. Descargar el JAR de la [página oficial](https://zipkin.io/).
2. Ejecutarlo con `java -jar zipkin-server-version-exec.jar`.
3. Acceder a la consola web en `http://localhost:9411/`.

## RabbitMQ
1. Descargar [Erlang](https://www.erlang.org/).
2. Descargar [RabbitMQ](https://www.rabbitmq.com/).
3. Ejecutar RabbitMQ como administrador.
4. Agregar dentro de la variable del sistema `Path` un nuevo registro apuntando a la carpeta `sbin` de RabbitMQ.
5. Acceder al terminal con permisos de administrador y lanzamos el comando `rabbitmq-plugins enable rabbitmq_management`.
6. Acceder a la consola web en `http://localhost:15672/`.
7. Para acceder introducir `guest` tanto en username como en password.

## MySQL Storage
1. Crear nueva base de datos, ejemplo `zipkin`.
2. Crear usuario `zipkin` en la base de datos con contraseña `zipkin`.
3. Le damos permiso sobre los objetos en la base de datos `zipkin`.
4. Creamos el [schema](https://github.com/openzipkin/zipkin/blob/master/zipkin-storage/mysql-v1/src/main/resources/mysql.sql).
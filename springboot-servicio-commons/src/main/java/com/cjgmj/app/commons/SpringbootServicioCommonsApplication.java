package com.cjgmj.app.commons;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

// Importante quitar el método main y del pom el tag build
@SpringBootApplication
// Excluye la configuración de la conexión del datasource
@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class })
public class SpringbootServicioCommonsApplication {

}

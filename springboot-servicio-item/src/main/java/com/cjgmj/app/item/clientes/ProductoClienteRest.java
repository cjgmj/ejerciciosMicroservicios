package com.cjgmj.app.item.clientes;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.cjgmj.app.commons.models.entity.ProductoEntity;

@FeignClient(name = "servicio-productos")
public interface ProductoClienteRest {

	@GetMapping("/listar")
	public List<ProductoEntity> listar();

	@GetMapping("/ver/{id}")
	public ProductoEntity detalle(@PathVariable Long id);

	@PostMapping("/crear")
	public ProductoEntity crear(@RequestBody ProductoEntity producto);

	@PutMapping("/editar/{id}")
	public ProductoEntity update(@RequestBody ProductoEntity producto, @PathVariable Long id);

	@DeleteMapping("/eliminar/{id}")
	public void delete(@PathVariable Long id);

}

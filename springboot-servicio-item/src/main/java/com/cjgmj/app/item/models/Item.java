package com.cjgmj.app.item.models;

import com.cjgmj.app.commons.models.entity.ProductoEntity;

public class Item {

	private ProductoEntity producto;
	private Integer cantidad;

	public Item() {
		super();
	}

	public Item(ProductoEntity producto, Integer cantidad) {
		super();
		this.producto = producto;
		this.cantidad = cantidad;
	}

	public ProductoEntity getProducto() {
		return producto;
	}

	public void setProducto(ProductoEntity producto) {
		this.producto = producto;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Double getTotal() {
		return producto.getPrecio() * cantidad.doubleValue();
	}

}

package com.cjgmj.app.item.models.service;

import java.util.List;

import com.cjgmj.app.commons.models.entity.ProductoEntity;
import com.cjgmj.app.item.models.Item;

public interface IItemService {

	public List<Item> findAll();

	public Item findById(Long id, Integer cantidad);

	public ProductoEntity save(ProductoEntity producto);

	public ProductoEntity update(ProductoEntity producto, Long id);

	public void delete(Long id);

}

package com.cjgmj.app.item.models.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cjgmj.app.commons.models.entity.ProductoEntity;
import com.cjgmj.app.item.clientes.ProductoClienteRest;
import com.cjgmj.app.item.models.Item;
import com.cjgmj.app.item.models.service.IItemService;

@Service("serviceFeign")
public class ItemServiceFeignImpl implements IItemService {

	@Autowired
	private ProductoClienteRest clienteFeign;

	@Override
	public List<Item> findAll() {
		return clienteFeign.listar().stream().map(p -> new Item(p, 1)).collect(Collectors.toList());
	}

	@Override
	public Item findById(Long id, Integer cantidad) {
		return new Item(clienteFeign.detalle(id), cantidad);
	}

	@Override
	public ProductoEntity save(ProductoEntity producto) {
		return clienteFeign.crear(producto);
	}

	@Override
	public ProductoEntity update(ProductoEntity producto, Long id) {
		return clienteFeign.update(producto, id);
	}

	@Override
	public void delete(Long id) {
		clienteFeign.delete(id);
	}

}

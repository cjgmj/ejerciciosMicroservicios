package com.cjgmj.app.item.models.service.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.cjgmj.app.commons.models.entity.ProductoEntity;
import com.cjgmj.app.item.models.Item;
import com.cjgmj.app.item.models.service.IItemService;

@Service("serviceRestTemplate")
public class ItemServiceImpl implements IItemService {

	@Autowired
	private RestTemplate clienteRest;

	@Override
	public List<Item> findAll() {
		List<ProductoEntity> productos = Arrays
				.asList(clienteRest.getForObject("http://servicio-productos/listar", ProductoEntity[].class));

		return productos.stream().map(p -> new Item(p, 1)).collect(Collectors.toList());
	}

	@Override
	public Item findById(Long id, Integer cantidad) {
		Map<String, String> pathVariables = new HashMap<String, String>();
		pathVariables.put("id", id.toString());
		ProductoEntity producto = clienteRest.getForObject("http://servicio-productos/ver/{id}", ProductoEntity.class,
				pathVariables);
		return new Item(producto, cantidad);
	}

	@Override
	public ProductoEntity save(ProductoEntity producto) {
		HttpEntity<ProductoEntity> body = new HttpEntity<>(producto);

		ResponseEntity<ProductoEntity> response = clienteRest.exchange("http://servicio-productos/crear",
				HttpMethod.POST, body, ProductoEntity.class);

		ProductoEntity productoResponse = response.getBody();
		return productoResponse;
	}

	@Override
	public ProductoEntity update(ProductoEntity producto, Long id) {
		HttpEntity<ProductoEntity> body = new HttpEntity<>(producto);
		Map<String, String> pathVariables = new HashMap<String, String>();
		pathVariables.put("id", id.toString());

		ResponseEntity<ProductoEntity> response = clienteRest.exchange("http://servicio-productos/editar/{id}",
				HttpMethod.PUT, body, ProductoEntity.class, pathVariables);

		return response.getBody();
	}

	@Override
	public void delete(Long id) {
		Map<String, String> pathVariables = new HashMap<String, String>();
		pathVariables.put("id", id.toString());

		clienteRest.delete("http://servicio-productos/eliminar/{id}", pathVariables);
	}

}

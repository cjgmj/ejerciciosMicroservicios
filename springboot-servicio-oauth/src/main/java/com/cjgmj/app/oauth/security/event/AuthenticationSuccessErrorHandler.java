package com.cjgmj.app.oauth.security.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationEventPublisher;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.cjgmj.app.oauth.services.IUsuarioService;
import com.cjgmj.springboot.app.usuarios.commons.models.entity.Usuario;

import brave.Tracer;
import feign.FeignException;

@Component
public class AuthenticationSuccessErrorHandler implements AuthenticationEventPublisher {

	private static final Logger LOG = LoggerFactory.getLogger(AuthenticationSuccessErrorHandler.class);

	@Autowired
	private IUsuarioService usuarioService;

	@Autowired
	private Tracer tracer;

	@Override
	public void publishAuthenticationSuccess(Authentication authentication) {
		UserDetails user = (UserDetails) authentication.getPrincipal();
		LOG.info("Success Login: " + user.getUsername());

		Usuario usuario = usuarioService.findByUsername(authentication.getName());

		if (usuario.getIntentos() != null && usuario.getIntentos() > 0) {
			usuario.setIntentos(0);

			usuarioService.update(usuario, usuario.getId());
		}
	}

	@Override
	public void publishAuthenticationFailure(AuthenticationException exception, Authentication authentication) {
		LOG.error("Error en el Login: " + exception.getMessage());

		try {
			StringBuilder errors = new StringBuilder();
			errors.append("Error en el Login: " + exception.getMessage());

			Usuario usuario = usuarioService.findByUsername(authentication.getName());

			if (usuario.getIntentos() == null) {
				usuario.setIntentos(0);
			}

			usuario.setIntentos(usuario.getIntentos() + 1);

			LOG.info("Intentos fallidos: " + usuario.getIntentos());
			errors.append(" - Intentos fallidos: " + usuario.getIntentos());

			if (usuario.getIntentos() >= 3) {
				LOG.error(String.format("El usuario %s se ha deshabilitado por superar el máximo de intentos erróneos.",
						usuario.getUsername()));
				errors.append(" - "
						+ String.format("El usuario %s se ha deshabilitado por superar el máximo de intentos erróneos.",
								usuario.getUsername()));

				usuario.setEnabled(false);
			}

			usuarioService.update(usuario, usuario.getId());

			tracer.currentSpan().tag("error.mensaje", errors.toString());
		} catch (FeignException e) {
			LOG.error(String.format("El usuario %s no existe en el sistema", authentication.getName()));
		}
	}

}

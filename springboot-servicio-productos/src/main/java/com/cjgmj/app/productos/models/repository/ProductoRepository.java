package com.cjgmj.app.productos.models.repository;

import org.springframework.data.repository.CrudRepository;

import com.cjgmj.app.commons.models.entity.ProductoEntity;

public interface ProductoRepository extends CrudRepository<ProductoEntity, Long> {

}

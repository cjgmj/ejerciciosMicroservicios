package com.cjgmj.app.productos.models.service;

import java.util.List;

import com.cjgmj.app.commons.models.entity.ProductoEntity;

public interface IProductoService {

	public List<ProductoEntity> findAll();

	public ProductoEntity findById(Long id);

	public ProductoEntity save(ProductoEntity producto);

	public void deleteById(Long id);

}

package com.cjgmj.app.usuarios.models.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.cjgmj.springboot.app.usuarios.commons.models.entity.Usuario;

@RepositoryRestResource(path = "usuarios")
public interface UsuarioRepository extends PagingAndSortingRepository<Usuario, Long> {

	// La ruta a este método por defecto es
	// 'usuarios/search/findByUsername?username=user' con @RestResource
	// sobreescribimos el nombre del método por el path. Además también se puede
	// cambiar el nombre de la variable con @Param
	// De esta forma pasa de 'usuarios/search/findByUsername?username=user' a
	// 'usuarios/search/buscar-username?usuario=user'
	@RestResource(path = "buscar-username")
	public Usuario findByUsername(@Param("usuario") String username);

}

INSERT INTO USUARIOS (username, password, enabled, nombre, apellidos, email) VALUES ('joe', '$2a$10$vYh/.9FgpaiVlcqCtDXZMeOlNRoLu9mqM4utRyC.5qlBEEOiFG9AK', true, 'Joe', 'Simons', 'joe.simons@gmail.com');
INSERT INTO USUARIOS (username, password, enabled, nombre, apellidos, email) VALUES ('john', '$2a$10$wE4RNvLiyQXyIgAbXgIkq.C07csGcy6cg.fBr4RufFJhtgrybsIWS', true, 'John', 'Doe', 'john.doe@gmail.com');

INSERT INTO ROLES (nombre) VALUES ('ROLE_USER');
INSERT INTO ROLES (nombre) VALUES ('ROLE_ADMIN');

INSERT INTO USUARIOS_ROLES (usuario_id, role_id) VALUES (1, 1);
INSERT INTO USUARIOS_ROLES (usuario_id, role_id) VALUES (2, 2);
INSERT INTO USUARIOS_ROLES (usuario_id, role_id) VALUES (2, 1);
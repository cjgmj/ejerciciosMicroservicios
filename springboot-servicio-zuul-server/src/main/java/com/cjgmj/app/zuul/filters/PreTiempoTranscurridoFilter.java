package com.cjgmj.app.zuul.filters;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

@Component
public class PreTiempoTranscurridoFilter extends ZuulFilter {

	private static final Logger LOG = LoggerFactory.getLogger(PreTiempoTranscurridoFilter.class);

	// Indica las condiciones en la que se realizará el filtro
	@Override
	public boolean shouldFilter() {
		return true;
	}

	// Filtro que se realizará
	@Override
	public Object run() throws ZuulException {
		RequestContext ctx = RequestContext.getCurrentContext();
		HttpServletRequest request = ctx.getRequest();

		Long tiempoInicio = System.currentTimeMillis();
		request.setAttribute("tiempoInicio", tiempoInicio);

		LOG.info(String.format("%s request enrutado a %s", request.getMethod(), request.getRequestURL().toString()));

		return null;
	}

	// Indica el momento del ciclo de vida en el que se ejecutará el filtro ("pre",
	// "post", "route", "error")
	@Override
	public String filterType() {
		return "pre";
	}

	// Indica el orden del filtro
	@Override
	public int filterOrder() {
		return 1;
	}

}

@echo off
:: El puerto por defecto del servidor de RabbitMQ es 5672, a diferencia de la consola web que es 15672
set RABBIT_ADDRESSES=localhost:5672
:: Configuración para MySQL Storage
set STORAGE_TYPE=mysql
set MYSQL_USER=zipkin
set MYSQL_PASS=zipkin
java -jar ./zipkin-server-version-exec.jar